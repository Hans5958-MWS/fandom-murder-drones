import math
import os 
from PIL import Image
import imagehash
import cv2
from moviepy.editor import *
from tqdm import tqdm
from functools import cmp_to_key

hash_func = imagehash.average_hash
hash_bits = 32

hash_func_2 = hash_func
hash_bits_2 = 128


def pretty_duration(sec):
	secs = f"0{int(sec%60)}"[-2:]
	mins = f"0{int(sec/60)}"[-2:]
	ms = f"{sec % 1}"[1:4]
	if ms == ".0":
		ms = ""
	return f"{mins}:{secs}{ms}"

def binarySearch(data, val):
    lo, hi = 0, len(data) - 1
    best_ind = lo
    while lo <= hi:
        mid = lo + (hi - lo) // 2
        if data[mid] - val < 0:
            lo = mid + 1
        elif data[mid] - val > 0:
            hi = mid - 1
        else:
            best_ind = mid
            break
        # check if data[mid] is closer to val than data[best_ind] 
        if abs(data[mid] - val) < abs(data[best_ind] - val):
            best_ind = mid
    return best_ind

exclusion = """""".split('\n')

fps = 30

name_target = []
hashes_ref = []
hashes_ref_2 = []
hashes_target = []
hashes_target_2 = []
index = []

f = open('data/ref-hashes.txt', 'r', encoding='utf-8')
video_name = "source-vids/e1.mkv"
# video_cv2 = cv2.VideoCapture(video_name)
# fps = video_cv2.get(cv2.CAP_PROP_FPS)
video_moviepy = VideoFileClip(video_name)
fps = video_moviepy.reader.fps

# try:  
# 	if not os.path.exists('pet'): 
# 		os.makedirs('pet') 
# except OSError: 
# 	print ('Error') 

print("Calculating hashes on target...")
currentframe = 0
for f_path in tqdm(os.listdir('images/target/')):
	try:
		img = Image.open("images/target/" + f_path)
	except:
		continue
	aspect_ratio = img.width / img.height
	if abs(aspect_ratio - (16/9)) > 0.4:
		continue
	name_target.append(f_path)
	hashes_target.append(hash_func(img, hash_bits))
	hashes_target_2.append(hash_func_2(img, hash_bits_2))
	currentframe += 1
	# if not currentframe % 30:
	# 	print(currentframe)
print(len(hashes_target))

print("Loading reference video hashes...")
currentframe = 0
for line_raw in tqdm(f.readlines()):
	line = line_raw.strip()
	if line == "":
		break
	# print(line[:6])
	hashes_ref.append(imagehash.hex_to_hash(line))
	hashes_ref_2.append(None)
	currentframe += 1
	# if not currentframe % 30:
	# 	print(currentframe)
# print(len(hashes_ref))

# print("Sorting...")
# hashes_ref.sort(key=cmp_to_key(lambda x, y: x - y))

# index = list(range(len(hashes_ref)))

print("Comparing...")
index_target = 0
print()
compare_pbar = tqdm(hashes_target, total=len(hashes_target))
for hash_target in compare_pbar:
	# smallest_frame = binarySearch(hashes_ref, hash_target)
	# smallest_diff = hashes_ref[smallest_frame] - hash_target
	smallest_diff = math.inf
	frame_ref = 0
	index_ref = 0
	pass_indicies = []

	for hash_ref in hashes_ref:
		diff = abs(hash_target - hash_ref)
		if smallest_diff >= diff:
			if smallest_diff > diff:
				smallest_diff = diff
				pass_indicies.clear()
			print(f"\033[A\r\033[2K{name_target[index_target]} [2]: Frame {index_ref} ({len(pass_indicies)} frames) ({pretty_duration(index_ref/fps)}, {smallest_diff})", flush=True)
			pass_indicies.append(index_ref)
		index_ref += 1

	print(pass_indicies, "\n")

	smallest_diff = math.inf
	smallest_frame = -1
	frame_ref = 0
	
	for index_ref in pass_indicies:
		if hashes_ref_2[index_ref] == None:
			hashes_ref_2[index_ref] = hash_func(Image.fromarray(video_moviepy.get_frame(index_ref)), hash_bits_2)

		diff = abs(hashes_target_2[index_target] - hashes_ref_2[index_ref])
		if smallest_diff > diff:
			smallest_diff = diff
			smallest_frame = index_ref
			print(f"\033[A\r\033[2K{name_target[index_target]} [2]: Frame {smallest_frame} ({pretty_duration(smallest_frame/fps)}, {smallest_diff}{', PASS' if smallest_diff < 3000 else ''})", flush=True)

	if smallest_diff < 3000:
		try:
			pass
			# shutil.copyfile("images/target/" + name_target[index_target], "orig/" + name_target[index_target])
			
			# cv2
			# video_cv2.set(cv2.CAP_PROP_POS_FRAMES, smallest_frame)
			# ret, frame = video_cv2.read()
			# cv2.imwrite("images/out/1" + name_target[index_target], frame)

			# ffmpeg
			# print(['ffmpeg', '-ss', str((smallest_frame - 1)/fps), '-i', video_name, '-frames:v', '1', '-q:v', '1', '-vf', 'scale=out_color_matrix=bt709', '-color_primaries', 'bt709', '-color_trc', 'bt709', '-colorspace', 'bt709', "images/out/2" + name_target[index_target], '-y'])
			# subprocess.run(['ffmpeg', '-ss', str(smallest_frame/fps), '-i', video_name, '-frames:v', '1', '-q:v', '1', '-vf', 'scale=out_color_matrix=bt709', '-color_primaries', 'bt709', '-color_trc', 'bt709', '-colorspace', 'bt709', "images/out/2" + name_target[index_target], '-y'])
			
			# moviepy
			video_moviepy.save_frame("images/out/3" + name_target[index_target], smallest_frame/fps)
		except:
			print(f"\033[A\r\033[2KFailed to write {name_target[index_target]}!")
	index_target += 1

f.close()
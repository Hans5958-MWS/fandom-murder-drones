var files = `1029202131.png
12015017201290.png
12015017201293.png
1734215777250955.png
4243180.png
4243181.png
4243182.png
Braxton1.png
Bythpnkdmn452df164wmpr.png
Copper9.png
Disassembly required.jpeg
FMDhsm7WYAQINag.jpg
Goji1.jpg
Hans.png
Human.png
I have no clue at all.png
Im not disappointed, im very disappointed.png
J3.png
Khan2.png
Md season 1 logo.png
Md3.png
Moon.png
N t-poseing.jpg
N16.png
N7.png
Photograph1.png
Salute.png
Screenshot 2022-04-26 6.40.01 PM.png
Symbol 2.png
Symbol 3.png
Symbol 4.png
THIS IS AWESOME.png
Uzi Yellow Glitch.png
Uzi2.png
Uzi4.png
UziBlush.png
V4.png
V5.png
V7.png
Veg.png
Veg2.png
VNM-16 Custom Image.jpg
WD1.png
WD2.png
WD3.png
WD5.png
WD6.png
Yannis.png`.split('\n')

// const videoEl = document.querySelector('video')
// videoEl.currentTime = 3923 / 30

let fileIndex = 0

const updateImage = () => {
	const playerWrapperEl = document.querySelector('.player-wrapper')
	playerWrapperEl.classList.remove('wipe-in')
	setTimeout(() => playerWrapperEl.classList.add('wipe-in'), 100)
	if (fileIndex < 0) fileIndex = files.length - 1
	else if (fileIndex >= files.length) fileIndex = 0
	const [imgEl1, imgEl2] = document.querySelectorAll('img')
	imgEl1.src = "../images/target/" + files[fileIndex]
	imgEl2.src = "../images/out/" + files[fileIndex]
	document.querySelector('h2').textContent = files[fileIndex]
}

updateImage()

document.addEventListener('keydown', (e) => {
	e = e || window.event;
	if (e.key === 'ArrowRight') {
		fileIndex++
		updateImage()
	} else if (e.key === 'ArrowLeft') {
		fileIndex--
		updateImage()
	}
})

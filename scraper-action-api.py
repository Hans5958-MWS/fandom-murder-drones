import os
import shutil
import requests
import urllib
from sanitize_filename import sanitize
import multiprocessing

url = "https://murder-drones.fandom.com/api.php?action=query&format=json&prop=imageinfo&list=&iiprop=url&titles="

def process(file_page):
	file_page = file_page.strip()
	file_name = file_page[5:]
	if (os.path.exists("images/target/" + sanitize(file_name))):
		return
	
	req = requests.get(url + urllib.parse.quote("File:" + file_name)).json()

	if req['query']['pages']:
		file_url = req['query']['pages'][list(req['query']['pages'].keys())[0]]['imageinfo'][0]['url']
		with urllib.request.urlopen(file_url) as response, open("images/target/" + sanitize(file_name), 'wb') as out_file:
			shutil.copyfileobj(response, out_file)
		print(file_name + ": " + file_url)

	else:
		print(file_name + ": Link not found")

if __name__ == '__main__':
	
	f = open('data/files.txt', 'r', encoding='utf-8')

	pool = multiprocessing.Pool()
	pool = multiprocessing.Pool(processes=8)

	pool.map(process, f.readlines())

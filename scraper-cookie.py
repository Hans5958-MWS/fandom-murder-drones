import os
import shutil
import requests
from bs4 import BeautifulSoup
import urllib

cookies = {
	'fandom_session': 'SESSION_COOKIE'
}

url = "https://murder-drones.fandom.com/wiki/"
f = open('data/files.txt', 'r', encoding='utf-8')
pages = 0

while True:
	file_page = f.readline().strip()
	file_name = file_page[5:]
	try:
		if (os.path.exists("images/target/" + file_name)):
			continue
		soup = BeautifulSoup(requests.get(url + file_page, cookies=cookies).content, "html.parser")
		results = soup.select_one('a.internal')
		if results:
			file_url = results.attrs['href']
			with urllib.request.urlopen(file_url) as response, open("images/target/" + file_name, 'wb') as out_file:
				shutil.copyfileobj(response, out_file)
			print(file_name + ": " + file_url)
		else:
			print(file_name + ": Link not found")
		if file_page == "":
			break
	except:
		print(file_name + ": Error occured")

f.close()
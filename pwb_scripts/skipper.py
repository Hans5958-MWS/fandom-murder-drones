#!/usr/bin/python3
"""
A script that skips all the pages to print the. Use the generator and filters 
to customize what pages you want.

The following generators and filters:

&params;
"""
#
# (C) Pywikibot team, 2006-2022
#
# Distributed under the terms of the MIT license.
#
import pywikibot
from pywikibot import pagegenerators
from pywikibot.bot import (
    ExistingPageBot,
    SingleSiteBot,
)
from pywikibot.page import Page

docuReplacements = {'&params;': pagegenerators.parameterHelp}

class BasicBot(
    SingleSiteBot,
    ExistingPageBot,
):
    def skip_page(self, page: Page) -> bool:
        """
        Prints the page title and skips it.
        """
        print(page.title().encode("utf-8").decode())
        return True

def main(*args: str) -> None:
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    :param args: command line arguments
    """
    options = {}
    local_args = pywikibot.handle_args(args)

    gen_factory = pagegenerators.GeneratorFactory()

    local_args = gen_factory.handle_args(local_args)

    for arg in local_args:
        arg, _, value = arg.partition(':')
        option = arg[1:]
        if option in ('summary', 'text'):
            if not value:
                pywikibot.input('Please enter a value for ' + arg)
            options[option] = value
        else:
            options[option] = True

    gen = gen_factory.getCombinedGenerator(preload=True)

    if not pywikibot.bot.suggest_help(missing_generator=not gen):
        bot = BasicBot(generator=gen, **options)
        bot.run()  # guess what it does


if __name__ == '__main__':
    main()

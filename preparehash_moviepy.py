import cv2 
import os 
import math
from PIL import Image
import imagehash
from tqdm import trange, tqdm
from moviepy.editor import *

hash_func = imagehash.average_hash
hash_bits = 128

hash = []

video_path = "source-vids/e1.mkv"
video_moviepy = VideoFileClip(video_path)
f = open('data/ref-hashes.txt', 'w', encoding='utf-8')

# try:  
# 	if not os.path.exists('pet'): 
# 		os.makedirs('pet') 
# except OSError: 
# 	print ('Error') 

def pretty_duration(sec):
	secs = f"0{int(sec%60)}"[-2:]
	mins = f"0{int(sec/60)}"[-2:]
	ms = f"{sec % 1}"[1:4]
	if ms == ".0":
		ms = ""
	return f"{mins}:{secs}{ms}"

currentframe = 0
framecount = video_moviepy.reader.nframes
fps = video_moviepy.reader.fps

range = tqdm(video_moviepy.iter_frames(), total=framecount)

for frame in range:
	frame_pil = Image.fromarray(frame)
	f.write(str(hash_func(frame_pil, hash_bits)) + '\n')
	currentframe += 1
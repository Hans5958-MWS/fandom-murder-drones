import cv2 
import os 
import math
from PIL import Image
import imagehash
from tqdm import trange, tqdm
from base64 import b64encode, b64decode

hash_func = imagehash.average_hash
hash_bits = 128

hash = []

video_path = "source-vids/e1.mkv"
video = cv2.VideoCapture(video_path)
f = open('data/ref-hashes.txt', 'w', encoding='utf-8')

# try:  
# 	if not os.path.exists('pet'): 
# 		os.makedirs('pet') 
# except OSError: 
# 	print ('Error') 

def pretty_duration(sec):
	secs = f"0{int(sec%60)}"[-2:]
	mins = f"0{int(sec/60)}"[-2:]
	ms = f"{sec % 1}"[1:4]
	if ms == ".0":
		ms = ""
	return f"{mins}:{secs}{ms}"

currentframe = 0
framecount = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
fps = video.get(cv2.CAP_PROP_FPS)

range = trange(framecount)

for i in range:
	ret, frame = video.read() 
	if ret:
		frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
		frame_pil = Image.fromarray(frame)

		hash_fin = str(hash_func(frame_pil, hash_bits))
		# hash_fin = b64encode(bytes.fromhex(str(imagehash.average_hash(frame_pil, 128)))).decode()
		f.write(hash_fin + '\n')
		currentframe += 1
		# if not currentframe % fps:
		# 	print(f"{currentframe}/{framecount} ({pretty_duration(currentframe/fps)}/{pretty_duration(framecount/fps)})")
	else: 
		break
video.release()

cv2.destroyAllWindows()
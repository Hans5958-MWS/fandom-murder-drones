# MWS - Murder Drones

This is the collection of scripts I use for editing in [murder-drones.fandom.com](https://murder-drones.fandom.com). This is mainly for updating the scene images with the higher quality provided on the YouTube video.

## Instructions

1. Install the requirements using `pip install -r requirements.txt` and prepare [Pywikibot](https://www.mediawiki.org/wiki/Manual:Pywikibot/Installation) (this can be done either before 2 or 8).
2. Get all the file names and put it on `data/files.txt` (e.g. `pwb skipper -start:File:! -ns:6 > data/files.txt`) 
3. Download all the images using `data/files.txt` and `scraper.py`. You will get `images/target/`, a folder containing all the images.
4. Download the video from YouTube. (this script uses `source-vids/e1.mkv`)
5. Prepare the hash file of the video using `preparehash.py` (be patient!). You will get `data/ref-hashes.txt`, a very big one.
6. Start comparing the frames with the downloaded images by its hash using `compare.py`. You will get `images/out/`, a folder containing the similar images.
7. Get the file names from the `images/out/` folder, put it on `comparer/index.js` or `comparer/grid.js`, and use your web browser to open `index.html` or `grid.js` to compare the images by eye.
8. Ensure Pywikibot is ready and run `upload-pwb.bat` to upload the images.

You can also adjust the bits on `preparehash.py` and `compare.py`, from 128 to something else (higher = more detailed hash), or the the algorithm based on the hashes provided by [ImageHash](https://pypi.org/project/ImageHash/).. You can also adjust the difference threshold on `compare.py` (less =  more exact the compared frame should be). 

Read the blog post: https://hans5958.github.io/blog/improving-the-murder-drones-wiki-screenshots/

## License

MIT
